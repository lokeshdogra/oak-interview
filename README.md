# OAK Interview Repository

![Static Badge](https://img.shields.io/badge/oak-your_energy_manager-blue)
![Static Badge](https://img.shields.io/badge/hiring_roles-frontend_%26_python_developer_and_data_analyst-8A2BE2?style=plastic)
![GitLab Contributors](https://img.shields.io/gitlab/contributors/56243558)
![GitLab Forks](https://img.shields.io/gitlab/forks/56243558)
![GitLab Issues](https://img.shields.io/gitlab/issues/all/56243558)
![GitLab License](https://img.shields.io/gitlab/license/56243558)


This repo contains the source code and asset needed to solve the question.

Note: Please don't commit into any branch instead fork the repo and make your changes and then commit to the repo. Please be innovative with your approach. This is a good chance for you to show your Git Skills, feel free to make changes to the ReadMe if you think necessary.

## Working Environment:

```sh {"id":"01HSZCCR8SEEKV7ZAP2HEVQE7X"}
Python Version: Python3.8
Install the libraries from the requirements.txt
```

## How to Run the Application [ if you don't know python]

#### Python Virtual Environment Creation &nbsp; `python -m venv interview-env`

- For Windows `interview-env\Scripts\activate`
- For Unix or MacOS `source interview-env/bin/activate`

#### Install Libraries &nbsp; `python -m pip install -r requirements.txt`

#### Steps to Run the Application

```sh {"id":"01HSZCCR8SEEKV7ZAP2JPAS6D8"}
cd frontend-interview-test
python app.py
```

# Connect with us
<img src='assets/icons8-facebook-24.png' height=50px href="https://www.facebook.com/TheOakNetwork"></img>
<img src='assets/icons8-instagram-48.png' height=50px href="https://www.instagram.com/theoaknetwork/"></img>
<img src='assets/icons8-linkedin-50.png' height=50px href="https://www.linkedin.com/company/the-oak-network/"></img>
<img src='assets/icons8-twitterx-24.png' height=50px href="https://twitter.com/TheOAKNetwork"></img>
<img src='assets/icons8-youtube-32.png' height=50px href="https://www.youtube.com/@theoaknetwork4096"></img>


<img src="OAK.png" width="200" title="hover text" href="https://oak-network.com">
